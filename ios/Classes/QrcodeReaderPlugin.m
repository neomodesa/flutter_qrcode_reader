#import "QrcodeReaderPlugin.h"
#if __has_include(<qrcode_reader/qrcode_reader-Swift.h>)
#import <qrcode_reader/qrcode_reader-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "qrcode_reader-Swift.h"
#endif

@implementation QrcodeReaderPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftQrcodeReaderPlugin registerWithRegistrar:registrar];
}
@end
