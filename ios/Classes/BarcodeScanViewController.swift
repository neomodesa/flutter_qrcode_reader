//
//  CameraViewController.swift
//  qrcode_reader
//
//  Created by Edison Santiago on 08/01/20.
//

import AVFoundation
import UIKit
import Flutter

class BarcodeScanViewControllerFactory: NSObject, FlutterPlatformViewFactory {
    var registrar: FlutterPluginRegistrar
    
    init(registrar: FlutterPluginRegistrar) {
        self.registrar = registrar
    }
    
    func create(withFrame frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?) -> FlutterPlatformView {
        return BarcodeScanViewController(
            withFrame: frame,
            viewIdentifier: viewId,
            arguments: args,
            registrar: self.registrar
        )
    }
}

class BarcodeScanViewController: NSObject, FlutterPlatformView {
    var viewId: Int64
    var mainView: ObservableView!
    var registrar: FlutterPluginRegistrar
    
    var channel: FlutterMethodChannel
    var currentResult: FlutterResult?
    
    var videoCaptureDevice: AVCaptureDevice!
    var metadataOutput = AVCaptureMetadataOutput()
    var previewLayer: AVCaptureVideoPreviewLayer?
    var captureSession = AVCaptureSession()
    
    init(withFrame frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?, registrar: FlutterPluginRegistrar) {
        
        self.viewId = viewId
        
        self.registrar = registrar
        
        self.channel = FlutterMethodChannel.init(name: "plugins.neomode.com.br/qrcode_reader_\(viewId)", binaryMessenger: registrar.messenger())
        
        super.init()
        
        self.channel.setMethodCallHandler {
            [weak self] (call, result) in
            self?.onMethodCall(call, result: result)
        }
        
        self.createView(frame: frame)
    }
    
    func createView(frame: CGRect) {
        let wrapperView = ObservableView(frame: frame)
        wrapperView.backgroundColor = UIColor.red
        wrapperView.onViewDidLayout = {
            view in
            self.previewLayer?.frame = view.bounds
        }
        self.mainView = wrapperView
    }
    
    func view() -> UIView {
        return mainView
    }
    
    func onMethodCall(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        if call.method == "scan#start" {
            //Start the camera
            self.startCamera()
            
            //Save the result handler so we can call it later when the code is scanned
            self.currentResult = result
        }
        if call.method == "scan#stop" {
            //Stop the camera
            self.stopCamera()
            
            if let currentResult = self.currentResult {
                //If there is a current result this means the scan#start channel was called
                //We must return nil to it
                currentResult(nil)
                //Clear the current result
                self.currentResult = nil
            }
            
            //The result for the scan#stop channel is always nil
            result(nil)
        }
    }
}

extension BarcodeScanViewController: AVCaptureMetadataOutputObjectsDelegate {
    func startCamera() {
        if self.captureSession.inputs.count == 0 {
            self.setupCamera()
        }
        
        if !self.captureSession.isRunning {
            self.captureSession.startRunning();
        }
    }
    
    func stopCamera() {
        if captureSession.isRunning {
            captureSession.stopRunning();
        }
    }
    
    func setupCamera() {
        let discoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .back)

        if discoverySession.devices.count > 0 {
            let input = try! AVCaptureDeviceInput(device: discoverySession.devices.first!)
                    
            if self.captureSession.canAddInput(input) {
                self.captureSession.addInput(input)
            }
            
            self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
            
            if let videoPreviewLayer = self.previewLayer {
                videoPreviewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                self.mainView.layer.addSublayer(videoPreviewLayer)
            }
            
            if self.captureSession.canAddOutput(self.metadataOutput) {
                self.captureSession.addOutput(self.metadataOutput)
                
                self.metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                self.metadataOutput.metadataObjectTypes = [.ean13, .qr]
            } else {
                print("Could not add metadata output")
            }
        }
        else {
            //No camera :(
            fatalError("No camera found :(")
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // This is the delegatem method called when a code is read
        for metadata in metadataObjects {
            if let readableObject = metadata as? AVMetadataMachineReadableCodeObject,                let code = readableObject.stringValue {
                
                //Vibrate to give feedback to user
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
                
                //Stop the camera
                if self.captureSession.isRunning {
                    self.captureSession.stopRunning()
                }
                
                self.onCodeScanned(code)
            }
        }
    }
    
    func onCodeScanned(_ code: String) {
        //Get the currentResult handler set by "scan#start" channel
        if let result = self.currentResult {
            result(code)
            self.currentResult = nil
        }
    }
}

class ObservableView: UIView {
    var onViewDidLayout: ((UIView) -> Void)?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.onViewDidLayout?(self)
    }
}
